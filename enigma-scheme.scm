#!/usr/bin/guile \
-e enigma-scheme -s
!#

;; enigma-scheme: An Enigma Machine Simulator
;;
;; Copyright (C) 2022 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(eval-when (expand load eval)
	   (let ((cf (current-filename)))
	     (when cf (add-to-load-path (dirname cf)))))

;; Allows calling tiny-machine through the shell, i.e. '-e enigma-scheme -s'
(define (enigma-scheme . args)
  (apply (module-ref (resolve-module '(enigma-scheme)) 'enigma-scheme) args))

(define-module (enigma-scheme)
  #:use-module (enigma-scheme rotor-tools)
  #:use-module (ice-9 match)
  #:use-module (ice-9 getopt-long)
  #:use-module (srfi srfi-43)
  #:export (enigma-scheme))

(define (enigma-scheme shell-args)
  (define (display-help)
    (display "\
enigma-scheme: An Enigma Machine Simulator

USAGE

  es [options] 

OPTIONS

  Program usage and information: 
  -h, --help              Show this help message and exit.
"))
  (let* ((option-spec `((help (value #f) (single-char #\h))))
	 (options (getopt-long shell-args option-spec))
	 (non-option-args (option-ref options '() #f)))
    (when (assq 'help options) (display-help) (exit))

    (let ((rotor (make-rotor '((#\A . #\B) (#\B . #\A)) #\A '(#\R))))
      (rotor 'print-char-map)
      (rotor 'offset)
      (display (rotor 'char-map #\A)) (newline))))
