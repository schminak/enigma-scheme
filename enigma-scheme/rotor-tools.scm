;; Copyright (C) 2022 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (enigma-scheme rotor-tools)
  #:use-module (enigma-scheme character-tools)
  #:use-module (ice-9 match)
  #:export (make-rotor))

(define alphabet-char-set (string->char-set "ABCDEFGHIJKLMNOPQRSTUVWXYZ"))

(define (make-rotor init-char-map init-offset notches)
  (define current-offset init-offset)
  (define char-map-alist
    (char-set-fold (lambda (c cma)
		     (cons (if (assoc c init-char-map)
			       (assoc c init-char-map)
			       (cons c c))
			   cma))
		   '()
		   alphabet-char-set))
  (define reverse-char-map-alist (map (lambda (p) (cons (cdr p) (car p))) char-map-alist))
  (define turnover-notches notches)
  (lambda rotor-operation
    (match
     rotor-operation
     (('turnover?) (if (memq current-offset turnover-notches) #t #f))
     (('turnover) (set! current-offset (add-chars #\A current-offset)))
     (('print-char-map) (display char-map-alist) (newline))
     (('char-map c) (assoc-ref char-map-alist (add-chars c current-offset)))
     (('offset) (display current-offset) (newline))
     (('turnover-notches) (display turnover-notches) (newline))
     (_ (display "Arg not given to rotor")))))

