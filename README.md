enigma-scheme: An Enigma Machine Simulator
==========================================

This project aspires to allow the simulation
of an arbitrarily expandable Enigma machine.
